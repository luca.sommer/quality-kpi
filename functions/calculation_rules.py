"""
File consists of several functions for the calculation rules of FAIR Quality KPIs
"""

from functions.classes import *

def test_function():
    """Test function to check module functionality"""
    print("You called the test function.")
    
# Neue Funktionen integriert, mit denen man die KPIs Masse, Preis und maximale Lieferzeit berechnen und sich ausgeben lassen kann. 

# new functions for calculating the mass

# Eine for-Schleife summiert die Massen aller Komponenten der Fahrzeuge auf und gibt die Gesamtmasse aus.

def kpi_mass(system: LegoAssembly)->float:
    # Add mass to assemblies   
    """
    Calculates the total mass of the system

    Args:
        system (LegoAssembly): LegoAssembly object that represents the system

    Returns:
        total_mass (float): Sum of masses of all components in the system in g

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than LegoAssembly).
    """
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_mass()")

    total_mass = 0
    for c in system.get_component_list(-1):
        total_mass += c.properties["mass [g]"]
    return total_mass # alternative: sum(c.properties["mass [g]"] for c in system.get_component_list(-1)) 

# new functions for calculating the price

# Eine for-Schleife summiert die Einzelpreise aller Komponenten der Fahrzeuge auf und gibt die Gesamtsumme aus.

def kpi_price(system: LegoAssembly)->float:
    
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_price()")

    price = 0
    for c in system.get_component_list(-1):
        price += c.properties["price [Euro]"]
    return price

# new functions for calculating the delivery time

# Die for-Schleife vergleicht alle Lieferzeiten der Bauteile und und gibt dann die maximale Lieferzeit aus.

def kpi_delivery_time(system: LegoAssembly)->float:
    
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_delivery_time()")

    delivery_time = 0
    for c in system.get_component_list(-1):
        if delivery_time < c.properties["delivery time [days]"]:
               delivery_time = c.properties["delivery time [days]"]   
    return delivery_time

if __name__ == "__main__":
    """
    Function to inform that functions in this module is
    intended for import not usage as script
    """
    print(
        "This script contains functions for calculating the FAIR Quality KPIs."
        "It is not to be executed independently."
    )
